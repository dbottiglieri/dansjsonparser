﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace ConsoleApp4
{
    class Program
    {
        public const string fileName = @"D:\Downloads\azureclilog.json";
        public const string outputFileName = @"D:\Downloads\outputtest3.txt";
        public const string node = @"name";
        public const string token = @"version";

        static void Main(string[] args)
        {
            var numberOfRecords = 0;
            using (StreamReader reader = File.OpenText(fileName))
            {
                Console.WriteLine($"{fileName} found. Finding \"{token}\" and writing to {outputFileName}...");
                var o = JToken.ReadFrom(new JsonTextReader(reader));
                var x = o.Children()[node].Where(s => s.Value<string>().Contains(token)).Select(c => c.Value<string>());
                numberOfRecords = x.Count();
                using (StreamWriter outputFile = new StreamWriter(outputFileName))
                {
                    foreach (string line in x)
                        outputFile.WriteLine(line);
                }
            }
            Console.WriteLine($"{numberOfRecords} records with the token \"{token}\" from {fileName} written to {outputFileName}.");
            Console.WriteLine("Press any key to close.");
            Console.ReadKey();
        }
    }
}
